<?php
if(!isset($_GET['page'])){header('Location:./?menu=mapel&page=1');}
require('lib/class.soal.inc.php');
$mapel = new banksoal();
$page = ($_GET['page']-1)*sph;
?>

<div class="container"> 
  <div class="row">
	  <form action="mapel-act.php" method="post" enctype="multipart/form-data" class="form-horizontal">
		<input type='hidden' name='opr' value='anyar' id='opr'>
	    <div class="form-group">
			<label class="col-sm-3">ID Mata Pelajaran</label>
			
			<div class="col-sm-9">
				<input type="text" name="idMapel" class="form-control" readOnly >
			</div>
	    </div>
	    <div class="form-group">
			<label class="col-sm-3">Nama Mata Pelajaran</label>
			
			<div class="col-sm-9">
				<input type="text" name="namaMapel" class="form-control" >
			</div>
	    </div>
	    	    
	    <div class="form-group">
			<label class="col-sm-3">&nbsp;</label>
			<div class="col-sm-9">
				<input type="submit" value="Tambah" class="btn btn-primary">
			</div>
	    </div>
	  </form>
  </div>
  <div class="row">
	  <p>DAFTAR NAMA MATA PALAJARAN</p>
		<table class="table table-sm">
			<thead>
				<tr>
					<th width='100'>No ID Mapel</th>
					<th>Nama Mata Pelajaran</th>
					<th width='100'>Tindakan</th>
				</tr>
			</thead>
			
			<tbody>
			<?php
				for($i=0; $i < count($mapel) ; $i++)
				{
					
					$mapel->mapelTampil($page);
					
				}
			?>
			<tr>
				<td width='100'> <a class='btn btn-primary form-control' onClick=prevPage() ><- Sebelumnya</a></td>
				<td id='pgnum' align='center'><?php echo $_GET['page']; ?></td>
				<td width='100'> <a class='btn btn-primary form-control' onClick=nextPage() >Berikutnya -></a></td>
			</tr>
			</tbody>
		</table>
  </div>
</div>


<script>
function chgMpl(mod,id)
{
	var mpid='#nama'+id;
	//var nmp=document.getElementById(mpid).innerHTML;
	var nmp=$(mpid).html();
	$("input[name='opr']").val('ganti');
	$("input[type='submit']").val('Edit');
	$("input[name='idMapel']").val(id);
	$("input[name='namaMapel']").val(nmp);
}
function rmvMpl(mod,id)
{
	var mpid='#nama'+id;
	//var nmp=document.getElementById(mpid).innerHTML;
	var nmp=$(mpid).html();
	var del = confirm('Mapel '+nmp+' akan dihapus ?');
	if(del == true){
		$.post('mapel-act.php', {
			id: id,
			upos: 'mapel',
			opr: 'buzex'
        });
	}
	location.reload();
}

function prevPage()
{
	var pn = $('#pgnum').html();
	var pp= parseInt(pn) - 1;
	if(pn==1)
	{
		window.location='./?menu=mapel&page=1';
	}else{
		window.location='./?menu=mapel&page='+pp;
	}
	
}

function nextPage()
{
	var pn = $('#pgnum').html();
	var np= parseInt(pn) + 1;
	window.location='./?menu=mapel&page='+np;
	
}
</script>
