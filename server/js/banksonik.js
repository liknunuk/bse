$('document').ready(function(){
  $('#btn-menu').click(function(){
		$('#menu-grp').show();
   });
   
   $('.menu-item').click(function(){
		$('#menu-grp').hide();
   });
   
   $('#upas2').focusout(function(){
	   if($('#upas1').val() != $('#upas2').val()){
		   alert('Kata Sandi Tidak Sama');
		   $('#upas1').val('');
		   $('#upas2').val('');
		   $('#upas1').focus();
	   }
   });
   
   $('#addUser').click(function(){
	   $('#userAdd').modal('show');
	   $.ajax({url:"forms/form-user.php?opr=anyar", success: function(result){
			$('#modal-body').html(result);
		}});
	});
	
	$('#addPkt').click(function(){
		$('#pktAdd').modal('show');
		$.ajax({url:"forms/form-pakso.php?opr=anyar", success: function(result){
			$('#modal-body').html(result);
		}});
	});
	
	$('#cariPaket').click(function(){
		var mp=$('#pimap').val();
		var kl=$('#pikel').val();
		var pn=$('#nohal').html();
		$.ajax({url:'pilPaket.php?mp='+mp+'&kl='+kl+'&pn='+pn,success:function(result){
			$('#paketSoal').html(result);
		}});
	});
});

function editUser(x){
	$('#userAdd').modal('show');
	$.ajax({url:"forms/form-user-edit.php?id="+x, success: function(result){
			$('#modal-body').html(result);
		}});
}

function busxUser(x){
	var cfr = confirm('Yakin akan menghapus user '+x);
	if(cfr == true){
		$.post('user-act.php', {
			id: x,
			upos: 'siswa',
			opr: 'buzex'
        });
	}
	location.reload();
}

function pwdUser(x,pos){
	$('#userAdd').modal('show');
	$.ajax({url:"forms/form-user-password.php?id="+x+"&upos="+pos, success: function(result){
			$('#modal-body').html(result);
		}});
}

function editPaket(id){
	$('#pktAdd').modal('show');
	$.ajax({url:"forms/form-pakso.php?opr=ganti&id="+id, success: function(result){
		$('#modal-body').html(result);
	}});
}

function buzxPaket(id){
	var cfr = confirm('Paket Soal ID:'+id+' akan dihapus');
	if(cfr == true){
		$.post('pakso-act.php',{
			opr: 'buzex',
			id: id
		});
	}
	location.reload();
}
