<?php
require('./lib/class.soal.inc.php');
$ps = new banksoal();
$soal = $ps->paketPungut($_GET['psid']);
?>
  <div class="row">
	  <div class="col-sm-12">
		  <table class="table table-sm">
			  <tr>
				  <td width="300px">MATA PELAJARAN</td>
				  <td>
					  <?php echo strtoupper($soal['mapel']); ?> 
					  KELAS <?php echo $soal['grade']; ?>
				  </td>
			  </tr>
			  
			  <tr>
				  <td>NOMOR PAKET</td><td><?php echo $soal['nmp']; ?></td>
			  </tr>
			  
			  <tr>
				  <td>TUTOR PENGUNGGAH</td><td><?php echo $soal['guru']; ?></td>
			  </tr>
		  </table>
	  </div>
  </div>
  
  <div class="row">
	  <form action="ssr.php" method="post" enctype="multipart/form-data" class="form-horizontal">
		<input type="hidden" name="psId" value="<?php echo $_GET['psid']; ?>" />
	    <div class="form-group">
			<label class="col-sm-2">NAMA BERKAS SOAL</label>
			<div class="col-sm-10">
				<input type="file" name="soal" class="btn btn-primary form-control">
			</div>
	    </div>
	    
	    
	    <div class="form-group">
			<label class="col-sm-2">&nbsp;</label>
			<div class="col-sm-10">
				<input type="submit" value="UNGGAH" class="btn btn-primary">
			</div>
	    </div>
	  </form>
  </div>
</div>
