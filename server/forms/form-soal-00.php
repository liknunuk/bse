<?php
require('./lib/class.soal.inc.php');
$soal = new banksoal();
$data = $soal->soalPungut($_GET['id']);
?>
<div class="container">
  <div class="row">
	  <form action="soal-act.php" method="post" enctype="multipart/form-data" class="form-horizontal">
	    <input type="hidden" name="id" class="form-control"  value="<?php echo $data['id']; ?>" readOnly>
	    <input type="hidden" name="psid" class="form-control"  value="<?php echo $data['psId']; ?>" readOnly>
	
	    <div class="form-group">
			<label class="col-sm-2">Nomor Soal</label>
			<div class="col-sm-10">
				<input type="text" name="ns" class="form-control"  value="<?php echo $data['nso']; ?>" readOnly>
			</div>
	    </div>
	    
	    <div class="form-group">
			<label class="col-sm-2">Pertanyaan</label>
			<div class="col-sm-10">
				<textarea class="form-control" id="pt" name="pt" rows="3"><?php echo $data['pertanyaan']; ?></textarea>
			</div>
	    </div>
	    
	    <div class="form-group">
			<label class="col-sm-2">Ilustrasi</label>
			<div class="col-sm-10">
				<div class="col-sm-3">
					<select id="ilusw" class="form-control">
						<option>Tidak Ada</option>
						<option>Lokal</option>
						<option>Link URL</option>
					</select>
				</div>
				<div  class="col-sm-9" id="ili">
					<input type='text' name='ili' value="<?php echo $data['ilustrasi']; ?>" class='form-control'>
				</div>
			</div>
	    </div>
	    
	    <div class="form-group">
			<label class="col-sm-2">Jawaban A</label>
			<div class="col-sm-10">
				<div class="col-sm-3">
					<select class="form-control" id="swA">
						<option>Text</option>
						<option>Image</option>
					</select>
				</div>
				<div class="col-sm-9" id="optA">
					<input type="text" name="optA" value="<?php echo $data['opta']; ?>" class="form-control">
				</div>
			</div>
	    </div>
	    
	    <div class="form-group">
			<label class="col-sm-2">Jawaban B</label>
			<div class="col-sm-10">
				<div class="col-sm-3">
					<select class="form-control" id="swB">
						<option>Text</option>
						<option>Image</option>
					</select>
				</div>
				<div class="col-sm-9" id="optB">
					<input type="text" name="optB" value="<?php echo $data['optb']; ?>" class="form-control">
				</div>
			</div>
	    </div>
	    
	    <div class="form-group">
			<label class="col-sm-2">Jawaban C</label>
			<div class="col-sm-10">
				<div class="col-sm-3">
					<select class="form-control" id="swC">
						<option>Text</option>
						<option>Image</option>
					</select>
				</div>
				<div class="col-sm-9" id="optC">
					<input type="text" name="optC" value="<?php echo $data['optc']; ?>" class="form-control">
				</div>
			</div>
	    </div>
	    
	    <div class="form-group">
			<label class="col-sm-2">Jawaban D</label>
			<div class="col-sm-10">
				<div class="col-sm-3">
					<select class="form-control" id="swD">
						<option>Text</option>
						<option>Image</option>
					</select>
				</div>
				<div class="col-sm-9" id="optD">
					<input type="text" name="optD" value="<?php echo $data['optd']; ?>" class="form-control">
				</div>
			</div>
	    </div>
	    
	    <div class="form-group">
			<label class="col-sm-2">Kunci Jawaban</label>
			<div class="col-sm-10">
				
				<select name='kunjaw' class='form-control'>
					<option value='opta'>Jawaban A</option>
					<option value='optb'>Jawaban B</option>
					<option value='optc'>Jawaban C</option>
					<option value='optd'>Jawaban D</option>
				</select>
			</div>
	    </div>
	    
	    <div class="form-group">
			<label class="col-sm-2">&nbsp;</label>
			<div class="col-sm-10">
				<input type="submit" value="Edit" class="btn btn-primary">
			</div>
	    </div>
	  </form>
  </div>
</div>

<script>
$(document).ready(function(){
	$("#ilusw").on("change",function(){
		var ils = $("#ilusw").val();
		if(ils == 'Tidak Ada'){
			$("#ili").html(" ");
		}else if(ils =='Link URL'){
			var ili="<input type='text' class='form-control' name='ili' placeHolder='Tulis/Copy url / lokasi file'>";
			$("#ili").html(ili);
		}else{
			var ili="<input type='file' class='btn btn-default form-control' name='ili'>";
			$("#ili").html(ili);
		}
	});
	
	$("#swA").on("change",function(){
		var sw=$("#swA").val();
		var opt="optA";
		var elm="#optA"
		swOpt(sw,opt,elm);
	});
	
	$("#swB").on("change",function(){
		var sw=$("#swB").val();
		var opt="optB";
		var elm="#optB"
		swOpt(sw,opt,elm);
	});
	
	$("#swC").on("change",function(){
		var sw=$("#swC").val();
		var opt="optC";
		var elm="#optC"
		swOpt(sw,opt,elm);
	});
	
	$("#swD").on("change",function(){
		var sw=$("#swD").val();
		var opt="optD";
		var elm="#optD"
		swOpt(sw,opt,elm);
	});
});

function swOpt(sw,opt,elm){
	var tx = "<input type='text' class='form-control' name="+opt+">";
	var fl = "<input type='file' class='form-control btn btn-default' name="+opt+">";
	if(sw == 'Text'){
		$(elm).html(tx);
	}else{
		$(elm).html(fl);
	}
}
</script>

  <!-- skrip tinimce -->
	<script type="text/javascript" src="tinymce/tinymce.min.js"></script>
	<script type="text/javascript">
		tinymce.init({
		selector: "#pt",
		height: 100,
		content_css : "css/content.css",
		plugins: [
			"advlist autolink lists link image charmap print preview anchor",
			"searchreplace visualblocks code fullscreen",
			"insertdatetime media table contextmenu paste"
		],
		toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
    
});

function sc(){
	console.debug(tinyMCE.activeEditor.getContent());
}
</script>
<!-- skrip tinimce -->
