<form class='form-horizontal' action='user-act.php' method='post'>
	<input type='hidden' id='opr' name='opr' value='<?php echo $_GET['opr'];?>'>
	<div class='form-group'>
		<label class='col-sm-3'>ID USER</label>
		<div class='col-sm-9'>
			<input class='form-control' type='text' id='uid' name='uid' value='' placeHolder='kode unik user'>
		</div>
	</div>
	
	<div class='form-group'>
		<label class='col-sm-3'>Nama Lengkap User</label>
		<div class='col-sm-9'>
			<input class='form-control' type='text' id='uname' name='uname' value=''>
		</div>
	</div>
	
	<div class='form-group'>
		<label class='col-sm-3'>Jenis Kelamin</label>
		<div class='col-sm-9'>
			<select id='usex' name='usex' class='form-control'>
				<option value='Laki-laki'>Laki-laki</option>
				<option value='Perempuan'>Perempuan</option>
			</select>
		</div>
	</div>
	
	<div class='form-group'>
		<label class='col-sm-3'>Kedudukan</label>
		<div class='col-sm-9'>
			<select id='upos' name='upos' class='form-control'>
				<option value='Siswa'>Siswa</option>
				<option value='Tutor'>Guru</option>
				<option value='Admin'>Admin</option>
			</select>
		</div>
	</div>
	
	<div class='form-group'>
		<label class='col-sm-3'>Kelas / Jenjang</label>
		<div class='col-sm-9'>
			<input class='form-control' type='text' id='ugra' name='ugra' value='0'>
		</div>
	</div>
	
	<div class='form-group'>
		<label class='col-sm-3'>Kata Sandi</label>
		<div class='col-sm-9'>
			<input class='form-control' type='password' id='upas1' name='upas1' value='12345_abc'>
		</div>
	</div>
	
	<div class='form-group'>
		<label class='col-sm-3'>Konfirmasi Sandi</label>
		<div class='col-sm-9'>
			<input class='form-control' type='password' id='upas2' name='upas2' value='12345_abc'>
		</div>
	</div>
	
	<div class='form-group'>
		<label class='col-sm-3'>&nbsp;</label>
		<div class='col-sm-9 ratakanan'>
			<input type='submit' class='btn btn-primary' value='Simpan'>
			<input type='reset'  class='btn btn-warning' value='Ulang'>
		</div>
	</div>
</form>
