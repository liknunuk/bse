<?php
	require('../lib/class.gurusiswa.inc.php');
	$user = new gurusiswa();
	$data = $user->userPungut($_GET['id']);
?>
<form class='form-horizontal' action='user-act.php' method='post'>
	<input type='hidden' id='opr' name='opr' value='ganti'>
	<div class='form-group'>
		<label class='col-sm-3'>ID USER</label>
		<div class='col-sm-9'>
			<input class='form-control' type='text' id='uid' name='uid' 
				value=<?php echo $_GET['id']; ?> readOnly>
		</div>
	</div>
	
	<div class='form-group'>
		<label class='col-sm-3'>Nama Lengkap User</label>
		<div class='col-sm-9'>
			<input class='form-control' type='text' id='uname' 
			name='uname' value='<?php echo $data['nama']; ?>' />
		</div>
	</div>
	
	<div class='form-group'>
		<label class='col-sm-3'>Jenis Kelamin</label>
		<div class='col-sm-9'>
			<select id='usex' name='usex' class='form-control'>
				<option value='Laki-laki' <?php if($data['sex'] =='Laki-laki'){ echo "selected"; } ?> >Laki-laki</option>
				<option value='Perempuan' <?php if($data['sex'] =='Perempuan'){ echo "selected"; } ?>>Perempuan</option>
			</select>
		</div>
	</div>
	
	<div class='form-group'>
		<label class='col-sm-3'>Kedudukan</label>
		<div class='col-sm-9'>
			<select id='upos' name='upos' class='form-control'>
				<option value='Siswa' <?php if($data['position'] =='Siswa'){ echo "selected"; } ?> >Siswa</option>
				<option value='Tutor' <?php if($data['position'] =='Tutor'){ echo "selected"; } ?> >Guru</option>
				<option value='Admin' <?php if($data['position'] =='Admin'){ echo "selected"; } ?> >Admin</option>
			</select>
		</div>
	</div>
	
	<div class='form-group'>
		<label class='col-sm-3'>Kelas / Jenjang</label>
		<div class='col-sm-9'>
			<input class='form-control' type='text' id='ugra' 
			name='ugra' value=<?php echo $data['grade']; ?> />
		</div>
	</div>
	
	<div class='form-group'>
		<label class='col-sm-3'>&nbsp;</label>
		<div class='col-sm-9 ratakanan'>
			<input type='submit' class='btn btn-primary' value='Simpan'>
			<input type='reset'  class='btn btn-warning' value='Ulang'>
		</div>
	</div>
</form>
