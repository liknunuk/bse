<?php
require('../lib/class.soal.inc.php');
$pakso = new banksoal();

if(isset($_GET['id'])){
	$ps = $pakso->paketPungut($_GET['id']);
}else{
	$ps = array('id'=>'','kmp'=>'','grade'=>'','nmp'=>'','kdg'=>'',
				'startDay'=>'','startHour'=>'','endDay'=>'','endHour'=>'');
}
 
?>
<form class='form-horizontal' action='pakso-act.php' method='post'>
	<input type='hidden' id='opr' name='opr' value='<?php echo $_GET['opr']; ?>' />

	<div class='form-group'>
		<label class='col-sm-3'>KODE BANK SOAL</label>
		<div class='col-sm-9'>
			<input type='text' name='idPaket' value='<?php echo $ps['id']; ?>' class='form-control' readOnly>
		</div>
	</div>
<?php if($_GET['opr']=='anyar'){ ?>	
	<div class='form-group'>
		<label class='col-sm-3'>MATA PELAJARAN</label>
		<div class='col-sm-9'>
			<select class='form-control' name='kmp'>
				<?php $pakso->selectMapel($ps['kmp']); ?>
			</select>
		</div>
	</div>

	<div class='form-group'>
		<label class='col-sm-3'>KELAS</label>
		<div class='col-sm-9'>
			<select class='form-control' name='grade' 
			onChange=cariGuru('<?php echo $ps['kdg']; ?>',this.value)>
				<option>Pilih Kelas</option>
				<?php $pakso->selectKelas($ps['grade']); ?>
			</select>
		</div>
	</div>
	
	<div id='nmptut'>
		<div class='form-group'>
			<label class='col-sm-3'>NOMOR PAKET</label>
			<div class='col-sm-9'>
				<input type='text' class='form-control' name='nmp' id='nmp' value='<?php echo $ps['nmp']; ?>' readOnly >
			</div>
		</div>
		
		<div class='form-group'>
			<label class='col-sm-3'>TUTOR</label>
			<div class='col-sm-9' id='tutorBS'>
				<select class='form-control' name='kdg'>
					<?php $pakso->selectTutor($ps['kdg'],$ps['grade']); ?>
				</select>
			</div>
		</div>
	</div>
<?php }else{ ?>
	</div>
	<div class='form-group'>
		<label class='col-sm-3'>MULAI</label>
		<div class='col-sm-9'>
		   <div class='col-sm-6'>
			   <input type='date' name='startDay' class='form-control' value='<?php echo $ps['startDay']; ?>'>
		   </div>
		   <div class='col-sm-6'>
			   <input type='text' name='startHour' class='form-control' value='<?php echo $ps['startHour']; ?>'>
		   </div>
		</div>
	</div>

	<div class='form-group'>
		<label class='col-sm-3'>SELESAI</label>
		<div class='col-sm-9'>
		   <div class='col-sm-6'>
			   <input type='date' name='endDay' class='form-control' value='<?php echo $ps['endDay']; ?>'>
		   </div>
		   <div class='col-sm-6'>
			   <input type='text' name='endHour' class='form-control' value='<?php echo $ps['endHour']; ?>'>
		   </div>
		</div>
	</div>
<?php } ?>
	<div class='form-group'>
		<label class='col-sm-3'>&nbsp;</label>
		<div class='col-sm-9 ratakanan'>
			<input type='submit' value='SimpaN' class='btn btn-primary'>
		</div>
	</div>
</form>

<script>
function cariGuru(tutor,grade)
{
	var idp = $("input[name='idPaket']").val();
	var kmp = $("select[name='kmp']").val();
	$.ajax({url:'cariTutor.php?key='+idp+','+kmp+','+tutor+','+grade, 
		    success: function(result){
			   $('#nmptut').html(result);   
		   }});
}
</script>
