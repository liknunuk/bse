<?php
	if(!isset($_GET['page'])){header("Location:./?menu=paket&page=1");}
	require('./lib/class.soal.inc.php');
	$ps = new banksoal();
?>
<div class='row'>
	<div class='col-sm-5'>
		<select id='pimap' class='form-control'>
			<option>Pilih Mata Pelajaran</option>
			<?php $ps->selectMapel();?>
		</select>
		<span style='display:none;' id='nohal'><?php echo $_GET['page'];?></span>
	</div>
	<div class='col-sm-5'>
		<select id='pikel' class='form-control'>
			<option>Pilih KELAS</option>
			<?php $ps->selectKelas(); ?>
		</select>
	</div>
	<div class='col-sm-2'>
		<a href=javascript:void(0) id='cariPaket' class='btn btn-info form-control'>Cari Paket</a>
	</div>
</div>
<table class='table table-sm'>
  <thead>
	<tr>
	   <td colspan='7'>
		   <a class='btn btn-primary' id='addPkt'>+ Paket</a>
	   </td>
	</tr>
    <tr>
      <th>MATA PELAJARAN</th>
      <th>KELAS</th>
      <th>PAKET</th>
      <th>GURU/TUTOR</th>
      <th>MULAI</th>
      <th>SELESAI</th>
      <th width='200px'>PILIHAN</th>
    </tr>
  </thead>
  <tbody id='paketSoal'>
  <?php $ps->paketLast5(); ?>
  </tbody>
</table>
<!-- Modal User Add-->
<div id="pktAdd" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Kelola Paket</h4>
      </div>
      <div class="modal-body" id="modal-body">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
