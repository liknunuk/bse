<!DOCTYPE html>
<html lang="en">
<head>
  <title>BANKSONIK</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
   <style type="text/css">
	  .ikonform{width:22px;}
  </style>
</head>
<body>

<div class="container">
  <div class="page-header">
	  <p>BANK SOAL ELEKTRONIK SEKOLAH .....</p>
	  <b>MATA PELAJARAN</b>
  </div>
 
  <div class="row">
	  <form action="mapel-act.php" method="post" enctype="multipart/form-data" class="form-horizontal">
	    <div class="form-group">
			<label class="col-sm-3">Nama Mata Pelajaran</label>
			<div class="col-sm-9">
				<input type="text" name="namaMapel" class="form-control" >
			</div>
	    </div>
	    	    
	    <div class="form-group">
			<label class="col-sm-3">&nbsp;</label>
			<div class="col-sm-9">
				<input type="submit" value="Tambah" class="btn btn-primary">
			</div>
	    </div>
	  </form>
  </div>
  <div class="row">
	  <p>DAFTAR NAMA MATA PALAJARAN</p>
		<table class="table table-sm">
			<thead>
				<tr>
					<th>No ID Mapel</th>
					<th>Nama Mata Pelajaran</th>
					<th>Tindakan</th>
				</tr>
			</thead>
			<?php
			$mapel = array(
				array('no'=>'1','nama'=>'Ilmu Pengetahuan Sosial'),
				array('no'=>'2','nama'=>'Ilmu Pengetahuan Alam'),
				array('no'=>'3','nama'=>'Matematika'),
				array('no'=>'4','nama'=>'Bahasa Indonesia'),
				array('no'=>'5','nama'=>'Bahasa Inggris'),
				array('no'=>'6','nama'=>'Pendidikan Kewarganegaraan')
			);
			?>
			<tbody>
			<?php
				for($i=0; $i < count($mapel) ; $i++)
				{
					echo "<tr>";
					
						echo "<td>".$mapel[$i]['no']."</td>";
						echo "<td id='nama".$mapel[$i]['no']."'>".$mapel[$i]['nama']."</td>";
						echo "<td>
						<a href=javascript:void(0)><img src='ikonze/chg.png' class='ikonform' id='".$mapel[$i]['no']."' onClick=chgMpl('edt',this.id) /></a>
						<a href=javascript:void(0)><img src='ikonze/rmv.png' class='ikonform' id='".$mapel[$i]['no']."' onClick=rmvMpl('rmv',this.id) /></a>
						</td>
					</tr>
				";
				}
			?>
			</tbody>
		</table>
  </div>
</div>

</body>
</html>
<script>
function chgMpl(mod,id)
{
	var mpid='#nama'+id;
	//var nmp=document.getElementById(mpid).innerHTML;
	var nmp=$(mpid).html();
	$("input[name='namaMapel']").val(nmp);
}
function rmvMpl(mod,id)
{
	var mpid='#nama'+id;
	//var nmp=document.getElementById(mpid).innerHTML;
	var nmp=$(mpid).html();
	var del = confirm('Mapel '+nmp+' akan dihapus ?');
	if(del == true){
		alert('berhasil dihapus');
	}else{
		alert('batal dihapus');
	}
}
</script>
