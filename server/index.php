<!DOCTYPE html>
<html lang="en">
<head>
  <title>BANKSONIK</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <!--
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  -->
  <link rel="stylesheet" href="css/banksonik.css">
  <script src="js/banksonik.js"></script>
</head>
<body>

<div class="container">
  <div class="page-header">
	  <p>BANK SOAL ELEKTRONIK SMK PANCA BHAKTI RAKIT BANJARNEGARA</p>
  </div>
  <div>
	  <a href=javascript:void(0) class='btn btn-primary' id='btn-menu'>Menu Bank Soal</a>
	  <div class='list-group' id='menu-grp'>
		  <a class='list-group-item menu-item' href='./?menu=tutor'>TUTOR</a>
		  <a class='list-group-item menu-item' href='./?menu=siswa'>SISWA</a>
		  <a class='list-group-item menu-item' href='./?menu=mapel'>MAPEL</a>
		  <a class='list-group-item menu-item' href='./?menu=paket'>PAKET</a>
		  <!--a class='list-group-item menu-item' href='./?menu=soal'>SOAL</a-->
	  </div>
  </div>
  <div class="row">
  <?php
  
    if(!isset($_GET['menu']))
    {
		echo "
		<script>window.location='./?menu=siswa';</script>
		";
	}else{
		$ht = kamusmenu($_GET['menu']);
		echo "<h3>MANAGEMEN DATA ".strtoupper($ht)."</h3>";
		switch(strtolower($_GET['menu'])){
			case 'tutor': include('user.php'); break;
			case 'siswa': include('user.php'); break;
			case 'mapel': include('mapel.php'); break;
			case 'paket': include('paket.php'); break;
			case 'soal' : include('lembarSoal.php'); break;
			case 'upso' : include('upload_soal.php'); break;
			case 'puso' : include('forms/form-soal.php'); break;
			case 'logls' : include('logls.php'); break;
			case 'repak' : include('rekPaket.php'); break;
			default: include('user.php'); break;
		}
	}
  ?>
  </div>
</div>
</body>
</html>
<?php
function kamusmenu($data){
	switch($data){
		case 'tutor' : $ht = 'guru / tutor'; break;
		case 'siswa' : $ht = 'siswa / peserta'; break;
		case 'mapel' : $ht = 'mata pelajaran'; break;
		case 'paket' : $ht = 'paket soal'; break;
		case 'soal'  : $ht = 'soal'; break;
		case 'upso'  : $ht = 'upload soal'; break;
		case 'puso'  : $ht = 'ubah soal'; break;
		case 'logls' : $ht = 'Log Latihan Soal'; break;
		case 'repak' : $ht = 'Rekap Hasil Try Out' ; break;
	}
	return($ht);
}
?>
