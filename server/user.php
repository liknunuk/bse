<?php
$menu=$_GET['menu'];
if(!isset($_GET['page'])){header("Location:./?menu=".$menu."&page=1");}
require('lib/class.gurusiswa.inc.php');
$upos=$_GET['menu'];
$user = new gurusiswa();
$page = ($_GET['page']-1)*bph;
?>
<div>
	<a class='btn btn-primary' id='addUser'>+ Pengguna</a>
</div>
<table class='table table-sm'>
	<thead>
		<tr>
			<th>NAMA LENGKAP</th>
			<th>JENIS KELAMIN</th>
			<th>KEDUDUKAN</th>
			<th>KELAS</th>
			<th width='200'>PILIHAN</th>
		</tr>
	</thead>
	<tbody>
	<?php
	$user->userTampil($page,$upos);
	?>
	</tbody>
</table>
<table width='100%'>
	<tr>
		<td width='100'> <a class='btn btn-primary form-control' onClick=prevPage('<?php echo $menu; ?>') ><- Sebelumnya</a></td>
		<td id='pgnum' align='center'><?php echo $_GET['page']; ?></td>
		<td width='100'> <a class='btn btn-primary form-control' onClick=nextPage('<?php echo $menu; ?>') >Berikutnya -></a></td>
	</tr>
</table>
<!-- Modal User Add-->
<div id="userAdd" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Kelola User</h4>
      </div>
      <div class="modal-body" id="modal-body">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script>
function prevPage(menu)
{
	var pn = $('#pgnum').html();
	var pp= parseInt(pn) - 1;
	if(pn==1)
	{
		window.location='./?menu='+menu+'&page=1';
	}else{
		window.location='./?menu='+menu+'&page='+pp;
	}
	
}

function nextPage(menu)
{
	var pn = $('#pgnum').html();
	var np= parseInt(pn) + 1;
	window.location='./?menu='+menu+'&page='+np;
	
}
</script>
