<?php
define("sph",25);
class banksoal
{
	function transact($sql)
	{
		include("koneksi.inc.php");
		$qry = $conn->prepare($sql);
		return $qry;
	}
	
	/* *** PAKET SOAL *** */
	
	function paketNambah($kmp,$grd,$nmp,$kdg,$sd,$sh,$ed,$eh){
		$sql = "INSERT INTO pakso
				SET	kmp = ? , grade = ? , nmp = ? , kdg = ? ,
					startDay = ? , startHour = ? , endDay = ? ,
					endHour = ?";
		$qry = $this->transact($sql);
		$qry->execute(array($kmp,$grd,$nmp,$kdg,$sd,$sh,$ed,$eh));
		$qry = null;
	}
	
	function paketNgubah($id,$sd,$sh,$ed,$eh){
		$sql = "UPDATE pakso
				SET	startDay = ? , startHour = ? , endDay = ? ,
					endHour = ?
				WHERE id = ? ";
		$qry = $this->transact($sql);
		$qry->execute(array($sd,$sh,$ed,$eh,$id));
		$qry = null;
	}
	
	function paketMusnah($id){
		$sql = "DELETE FROM pakso
				WHERE id = ? 
				LIMIT 1";
		$qry = $this->transact($sql);
		$qry->execute(array($id));
		$qry = null;
	}
	
	function paketTampil($mp,$kls,$baris){
		$sql = "SELECT * 
				FROM view_paketsoal
				WHERE mapel = (SELECT namaMapel from mapel WHERE id = ?) && grade = ?
				LIMIT $baris,".sph;
		$qry = $this->transact($sql);
		$qry->execute(array($mp,$kls));
		while($rs = $qry->fetch())
		{
			echo "
			<tr>
			   <td>".$rs['mapel']."</td>
			   <td>".$rs['grade']."</td>
			   <td>".$rs['nmp']."</td>
			   <td>".$rs['guru']."</td>
			   <td>".$rs['startDay']."/".$rs['startHour']."</td>
			   <td>".$rs['endDay']."/".$rs['endHour']."</td>
			   <td>
				<a href=javascript:void(0) onClick=editPaket('".$rs['id']."')>
					<img src='ikonz/Edt16.png' class='tombol'>
				</a>
				<a href=javascript:void(0) onClick=buzxPaket('".$rs['id']."')>
					<img src='ikonz/Del16.png' class='tombol'>
				</a>
				<a href='./?menu=upso&psid=".$rs['id']."'>
					<img src='ikonz/Add16.png' class='tombol'>
				</a>
				<a href='./?menu=soal&psid=".$rs['id']."'>
					<img src='ikonz/Mon16.png' class='tombol'>
				</a>
			   </td>
			</tr>";
		}
		$qry = null;
	}
	
	function paketLast5(){
		$sql = "SELECT * FROM view_paketsoal
				ORDER BY id DESC
				LIMIT 5";
		$qry = $this->transact($sql);
		$qry->execute();
		while($rs = $qry->fetch())
		{
			echo "
			<tr>
			   <td>".$rs['mapel']."</td>
			   <td>".$rs['grade']."</td>
			   <td>".$rs['nmp']."</td>
			   <td>".$rs['guru']."</td>
			   <td>".$rs['startDay']."/".$rs['startHour']."</td>
			   <td>".$rs['endDay']."/".$rs['endHour']."</td>
			   <td>
				<a href=javascript:void(0) onClick=editPaket('".$rs['id']."')>
					<img src='ikonz/Edt16.png' class='tombol'>
				</a>
				<a href=javascript:void(0) onClick=buzxPaket('".$rs['id']."')>
					<img src='ikonz/Del16.png' class='tombol'>
				</a>
				<a href='./?menu=upso&psid=".$rs['id']."'>
					<img src='ikonz/Add16.png' class='tombol'>
				</a>
				<a href='./?menu=soal&psid=".$rs['id']."'>
					<img src='ikonz/Mon16.png' class='tombol'>
				</a>
				<a href='./?menu=repak&psid=".$rs['id']."'>
					<img src='ikonz/Mon16.png' class='tombol'>
				</a>
			   </td>
			</tr>";
		}
		$qry = null;
	}
	
	function paketPungut($id){
		$sql = "SELECT * FROM view_paketsoal WHERE id = ? LIMIT 1";
		$qry = $this->transact($sql);
		$qry->execute(array($id));
		$rs = $qry->fetch();
		return($rs);
		$qry = null;
	}
	
	function paketAnyar($kmp,$kls){
		$sql = "SELECT MAX(nmp) nmp FROM pakso
				WHERE kmp= ? && grade = ?";
		$qry = $this->transact($sql);
		$qry->execute(array($kmp,$kls));
		$rs = $qry->fetch();
		$nmp = $rs['nmp']+1;
		return $nmp;
	}
	
	function nomorPaket($idp){
		$sql = "SELECT nmp FROM pakso
				WHERE id = ?";
		$qry = $this->transact($sql);
		$qry->execute(array($id));
		$rs = $qry->fetch();
		return $rs['nmp'];
	}
	
	/* *** PAKET SOAL *** */
	
	/* *** BANK SOAL *** */
	
	function soalNambah($ps,$ns,$pt, $il, $oa,$ob,$oc,$od,$oe,$kj){
		$sql="INSERT INTO bankso 
			  SET 	psId= ? , nso= ? , pertanyaan = ? , 
					ilustrasi= ? , opta= ? , optb= ? ,	optc= ? ,
					optd= ? , opte = ?, kunjaw = ? ";
		$qry = $this->transact($sql);
		$qry->execute(array($ps,$ns,$pt,$il, $oa,$ob,$oc,$od,$oe, $kj));
		$qry = null;			
	}
	
	function soalNgubah($kd,$ps,$ns,$pt,$il,$oa,$ob,$oc,$od,$kj){
		$sql="UPDATE bankso 
			  SET 	psId= ? , nso= ? , pertanyaan = ? , 
					ilustrasi= ? , opta= ? , optb= ? ,	optc= ? ,
					optd= ? , kunjaw = ? 
			  WHERE id = ?";
		$qry = $this->transact($sql);
		$qry->execute(array($ps,$ns,$pt,$il,$oa,$ob,$oc,$od,$kj,$kd));
		$qry = null;
	}
	
	
	function soalMushah($id){
		$sql="DELETE FROM bankso 
			  WHERE id = ?
			  LIMIT 1";
		$qry = $this->transact($sql);
		$qry->execute(array($id));
		$qry = null;
	}
	
	function soalTampil($ps){
		$sql="	SELECT * FROM bankso
				WHERE psId = ?";
		$qry = $this->transact($sql);
		$qry->execute(array($ps));
		while($rs=$qry->fetch())
		{
			echo "
			<table class='table' border='0' cellspacing='0' class='table'>
			  <tr><td width='15'>".$rs['nso']."</td><td colspan='2'>".$rs['pertanyaan']."</td></tr>";
			  
			if($rs['ilustrasi'] !=' '){
				echo "
				<tr>
			     <td>&nbsp;</td>
			     <td width='15'>&nbsp;</td>
			     <td class='ilustr'>".$rs['ilustrasi']."</td>
				</tr>";
			}
			 
			echo "
			   <tr>
			     <td>&nbsp;</td>
			     <td colspan='2'>
					<div class='row'>
					  <div class='col-sm-6'>
					  <table><tr><td valign='top'>A.&nbsp;</td>
					  <td class='piljaw'>".$rs['opta']."</td></tr></table>
					  </div>
					  <div class='col-sm-6'>
					  <table><tr><td valign='top'>B.&nbsp;</td>
					  <td class='piljaw'>".$rs['optb']."</td></tr></table>
					  </div>
					</div>
					<div class='row'>
					  <div class='col-sm-6'>
					  <table><tr><td valign='top'>C.&nbsp;</td>
					  <td class='piljaw'>".$rs['optc']."</td></tr></table>
					  </div>
					  <div class='col-sm-6'>
					  <table><tr><td valign='top'>D.&nbsp;</td>
					  <td class='piljaw'>".$rs['optd']."</td></tr></table>
					  </div>
					</div>
					<div class='row'>
					  <div class='col-sm-6'>
					  <table><tr><td valign='top'>E.&nbsp;</td>
					  <td class='piljaw'>".$rs['opte']."</td></tr></table>
					  </div>
					</div>
			     </td>
			   </tr>
			</table>
			<a class='btn btn-success' onClick=editSoal('".$rs['id']."')>Edit Soal Nomor ".$rs['nso']."</a>
			";

			/*
			$this->isImg('A',$rs['opta']);
			$this->isImg('B',$rs['optb']);
			$this->isImg('C',$rs['optc']);
			$this->isImg('D',$rs['optd']);
			
			echo "
			  <tr><td>&nbsp;</td><td width='15'>A</td><td class='piljaw'>".$rs['opta']."</td></tr>
			  <tr><td>&nbsp;</td><td width='15'>B</td><td class='piljaw'>".$rs['optb']."</td></tr>
			  <tr><td>&nbsp;</td><td width='15'>C</td><td class='piljaw'>".$rs['optc']."</td></tr>
			  <tr><td>&nbsp;</td><td width='15'>D</td><td class='piljaw'>".$rs['optd']."</td></tr>
			</table>
			";
			*/
		}
		$qry = null;
	}

	
	function soalPungut($id){
		$sql="	SELECT * FROM bankso
				WHERE id = ?";
		$qry = $this->transact($sql);
		$qry->execute(array($id));
		$rs=$qry->fetch();
		return($rs);
		$qry = null;
	}
	
	/* *** BANK SOAL *** */
	
	/* *** MATA PELAJARAN *** */
	
	function mapelNambah($nama){
		$sql = "INSERT INTO mapel SET namaMapel = ?";
		$qry = $this->transact($sql);
		$qry->execute(array($nama));
		$qry = null;
	}
	
	function mapelNgubah($id,$nama){
		$sql = "UPDATE mapel SET namaMapel = ? WHERE id = ?";
		$qry = $this->transact($sql);
		$qry->execute(array($nama,$id));
		$qry = null;
	}
	
	function mapelMusnah($id){
		$sql = "DELETE FROM mapel WHERE id = ? LIMIT 1";
		$qry = $this->transact($sql);
		$qry->execute(array($id));
		$qry = null;
	}
	
	function mapelTampil($baris){
		$sql = "SELECT * FROM mapel LIMIT $baris,".sph;
		$qry = $this->transact($sql);
		$qry->execute();
		while($rs = $qry->fetch()){
			echo "
			<tr>
			  <td>".$rs['id']."</td>
			  <td id='nama".$rs['id']."'>".$rs['namaMapel']."</td>
			  <td>
					<a href=javascript:void(0)><img src='ikonz/Edt16.png' class='tombol' id='".$rs['id']."' onClick=chgMpl('edt',this.id) /></a>
					<a href=javascript:void(0)><img src='ikonz/Del16.png' class='tombol' id='".$rs['id']."' onClick=rmvMpl('rmv',this.id) /></a>
				</td>
			</tr>
			";
		}
		$qry = null;
	}
	
	function mapelPungut($id){
		$sql = "SELECT * FROM mapel WHERE id= ?";
		$qry = $this->transact($sql);
		$qry->execute(array($id));
		$rs = $qry->fetch();
		return($rs);
		$qry = null;
	}
	
	/* *** MATA PELAJARAN *** */
	
	/* *** FORM SELECT *** */
	
	function selectMapel($kmp=''){
		$sql = "SELECT * FROM mapel ORDER BY namaMapel";
		$qry = $this->transact($sql);
		$qry->execute();
		while($rs = $qry->fetch())
		{
			if($rs['id']==$kmp){$sel='selected';}else{$sel='';}
			echo "<option $sel value='".$rs['id']."'>".$rs['namaMapel']."</option>";
		}
	}
	
	function selectKelas($grd=''){
		$sql = "SELECT DISTINCT(grade) FROM user ORDER BY grade";
		$qry = $this->transact($sql);
		$qry->execute();
		while($rs = $qry->fetch())
		{
			if($rs['grade']==$grd){$sel='selected';}else{$sel='';}
			echo "<option value='".$rs['grade']."'>".$rs['grade']."</option>";
		}
	}
	
	function selectTutor($kdg='',$grd=''){
		$sql = "SELECT id,nama,grade FROM user 
				WHERE position ='Tutor' && grade = ?
				ORDER BY nama";
		$qry = $this->transact($sql);
		$qry->execute(array($grd));
		while($rs = $qry->fetch())
		{
			if($rs['grade']==$grd){$sel='selected';}else{$sel='';}
			echo "<option value='".$rs['id']."'>".$rs['nama']."</option>";
		}
	}
	
	
	/* *** FORM SELECT *** */
	
	function paket5akhir($no){
		$sql = "SELECT * 
				FROM view_paketsoal
				WHERE id = ?
				LIMIT 5";
		$qry = $this->transact($sql);
		$qry->execute(array($no));
		$paketan=array();
		while($rs = $qry->fetch())
		{
			$paket = array ('m'=>$rs['mapel'],'k'=>$rs['grade'],
					'g'=>$rs['guru']);
			array_push($paketan,$paket);		
		}
		return $paketan;
		$qry = null;
	}
	
	/* *** CEK IMAGE *** */
	function isImg($ch,$str)
	{
		$pat = '/(img|src)/';
		preg_match($pat, $str, $matches);
		
		if($matches[0] == 'img'){
			echo "
			<tr><td>&nbsp;</td><td width='15'>".$ch."</td>
			<td class='piljaw'>".$str."</td>
			</tr>";
		}else{
			echo "<tr><td>&nbsp;</td><td width='15'>".$ch."</td>
			<td class='piljaw'>".$str."</td></tr>";
		}
		 
	}
	
	/* *** HISTORY *** */
	function trialHistory($uid){
		$sql = $this->transact("SELECT * FROM view_history 
				WHERE logId LIKE '$uid%'
				ORDER BY logId DESC
				LIMIT 60");
		$sql->execute();
		while($rs = $sql->fetch()){
			echo "
			<tr>
			  <td>".$rs['logId']."</td>
			  <td>".$rs['tanggal']."</td>
			  <td>".$rs['namaMapel']."</td>
			  <td>".$rs['nmp']."</td>
			  <td align='right' width='50'>".
			  number_format($rs['skore'],2,',','.')."</td>
			</tr>
			";
		}
	}
	/* *** HISTORY *** */
	/* *** REKAP PER PAKET *** */
	function paketRekap($id,$hal=0){
		$sql = "SELECT view_history.* , user.nama FROM view_history , user WHERE nmp = '{$id}' && user.id = left(view_history.logId,5) LIMIT $hal , 50";
		// echo $sql;
		
		$qry = $this->transact($sql);
		$qry->execute();
		//logId,psId,tanggal,skore
		while($rs = $qry->fetch()){
			/* logId,tanggal,namaMapel,nmp,skore */
			echo "
			<tr>
			  <td>".$rs['logId']."</td>
			  <td>".$rs['tanggal']."</td>
			  <td>".$rs['namaMapel']."</td>
			  <td>".$rs['nmp']."</td>
			  <td>".$rs['nama']."</td>
			  <td align='right' width='50'>".
			  number_format($rs['skore'],2,',','.')."</td>
			</tr>
			";
		}
	}
	/* *** REKAP PER PAKET *** */

	
}
?>
