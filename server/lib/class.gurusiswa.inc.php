<?php
define("bph",15);
class gurusiswa
{
	function transact($sql)
	{
		include("koneksi.inc.php");
		$qry = $conn->prepare($sql);
		return $qry;
	}
	
	/* *** USER *** */
	
	function userNambah($id,$nama,$lp,$pos,$pas,$gra){
		$pwd = md5($id."><".$pas);
		$sql = "INSERT INTO user
				SET id = ? , nama = ? , sex = ? , position = ? , 
					password = ? , grade = ?";
		$qry = $this->transact($sql);
		$qry->execute(array($id,$nama,$lp,$pos,$pwd,$gra));
		$qry =  null;
	}
	
	function userNgubahId($id,$nama,$lp,$pos,$gra){
		$sql = "UPDATE user 
				SET	nama = ? , sex = ? , position = ? , grade = ?
				WHERE id = ? ";
		$qry = $this->transact($sql);
		$qry->execute(array($nama,$lp,$pos,$gra,$id));
		$qry =  null;
	}
		
	function userNgubahPw($id,$pas){
		$pwd = md5($id."><".$pas);
		$sql = "UPDATE user SET password = ?
				WHERE id = ? ";
		$qry = $this->transact($sql);
		$qry->execute(array($pwd,$id));
		$qry =  null;
	}
	
	function userMusnah($id){
		$sql = "DELETE FROM user WHERE id = ?";
		$qry = $this->transact($sql);
		$qry->execute(array($id));
		$qry =  null;
	}
	
	function userTampil($baris,$upos){
		$sql =" SELECT * FROM user 
				WHERE position = ?
				LIMIT $baris,".bph;
		$qry = $this->transact($sql);
		$qry->execute(array($upos));
		while($rs = $qry->fetch())
		{
			echo "
			<tr>
			   <td>".$rs['nama']."</td>
			   <td>".$rs['sex']."</td>
			   <td>".$rs['position']."</td>
			   <td>".$rs['grade']."</td>
			   <td>
			     <a href=javascript:void(0) onClick=editUser('".$rs['id']."') >
			     <img class='tombol' src='ikonz/Edt16.png'></a>
			     <a href=javascript:void(0) onClick=busxUser('".$rs['id']."')>
			     <img class='tombol' src='ikonz/Del16.png'></a>
			     <a href=javascript:void(0) onClick=pwdUser('".$rs['id']."','".$rs['position']."')>
			     <img class='tombol' src='ikonz/Pwd16.png'></a>
			     </a>";
			if($upos=='siswa'){
				echo "<a class='tombol' href='./?menu=logls&id=".$rs['id']."'>Log</a>";
			}
			echo "     
			   </td>
			</tr>
			";
		}
		$qry =  null;
	}
	
	function userPungut($id){
		$sql ="SELECT * FROM user WHERE id = ?";
		$qry = $this->transact($sql);
		$qry->execute(array($id));
		$rs = $qry->fetch();
		return($rs);
		$qry =  null;
	}

	/* *** USER *** */
	
}
?>
