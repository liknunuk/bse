-- MySQL dump 10.13  Distrib 5.5.54, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: banksonik
-- ------------------------------------------------------
-- Server version	5.5.54-0+deb8u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bankso`
--

DROP TABLE IF EXISTS `bankso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bankso` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `psId` int(4) NOT NULL,
  `nso` int(2) NOT NULL,
  `pertanyaan` text,
  `ilustrasi` text,
  `opta` text,
  `optb` text,
  `optc` text,
  `optd` text,
  `kunjaw` varchar(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `history`
--

DROP TABLE IF EXISTS `history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `history` (
  `logId` varchar(12) DEFAULT NULL,
  `psId` int(3) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `skore` int(3) DEFAULT NULL,
  UNIQUE KEY `logId` (`logId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mapel`
--

DROP TABLE IF EXISTS `mapel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mapel` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `namaMapel` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pakso`
--

DROP TABLE IF EXISTS `pakso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pakso` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `kmp` int(3) NOT NULL,
  `grade` int(2) DEFAULT '7',
  `nmp` int(2) NOT NULL,
  `kdg` varchar(20) NOT NULL,
  `startDay` date DEFAULT NULL,
  `startHour` time DEFAULT NULL,
  `endDay` date DEFAULT NULL,
  `endHour` time DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` varchar(12) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `sex` enum('Laki-laki','Perempuan') DEFAULT 'Laki-laki',
  `position` enum('Siswa','Tutor','Admin') DEFAULT 'Siswa',
  `password` varchar(32) DEFAULT NULL,
  `grade` int(2) DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `view_history`
--

DROP TABLE IF EXISTS `view_history`;
/*!50001 DROP VIEW IF EXISTS `view_history`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_history` (
  `logId` tinyint NOT NULL,
  `tanggal` tinyint NOT NULL,
  `namaMapel` tinyint NOT NULL,
  `nmp` tinyint NOT NULL,
  `skore` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_paketsoal`
--

DROP TABLE IF EXISTS `view_paketsoal`;
/*!50001 DROP VIEW IF EXISTS `view_paketsoal`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_paketsoal` (
  `id` tinyint NOT NULL,
  `mapel` tinyint NOT NULL,
  `grade` tinyint NOT NULL,
  `nmp` tinyint NOT NULL,
  `guru` tinyint NOT NULL,
  `startDay` tinyint NOT NULL,
  `startHour` tinyint NOT NULL,
  `endDay` tinyint NOT NULL,
  `endHour` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `view_history`
--

/*!50001 DROP TABLE IF EXISTS `view_history`*/;
/*!50001 DROP VIEW IF EXISTS `view_history`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`banksoal`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_history` AS select `history`.`logId` AS `logId`,`history`.`tanggal` AS `tanggal`,`mapel`.`namaMapel` AS `namaMapel`,`pakso`.`nmp` AS `nmp`,`history`.`skore` AS `skore` from ((`history` join `mapel`) join `pakso`) where ((`pakso`.`id` = `history`.`psId`) and (`mapel`.`id` = `pakso`.`kmp`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_paketsoal`
--

/*!50001 DROP TABLE IF EXISTS `view_paketsoal`*/;
/*!50001 DROP VIEW IF EXISTS `view_paketsoal`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`banksoal`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_paketsoal` AS select `pakso`.`id` AS `id`,`mapel`.`namaMapel` AS `mapel`,`pakso`.`grade` AS `grade`,`pakso`.`nmp` AS `nmp`,`user`.`nama` AS `guru`,`pakso`.`startDay` AS `startDay`,`pakso`.`startHour` AS `startHour`,`pakso`.`endDay` AS `endDay`,`pakso`.`endHour` AS `endHour` from ((`pakso` join `mapel`) join `user`) where ((`mapel`.`id` = `pakso`.`kmp`) and (`user`.`id` = `pakso`.`kdg`) and (`user`.`position` = 'Tutor')) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-04-18 17:09:32
