-- MySQL dump 10.16  Distrib 10.1.26-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: banksonik
-- ------------------------------------------------------
-- Server version	10.1.26-MariaDB-0+deb9u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bankso`
--

DROP TABLE IF EXISTS `bankso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bankso` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `psId` int(4) NOT NULL,
  `nso` int(2) NOT NULL,
  `pertanyaan` text,
  `ilustrasi` text,
  `opta` text,
  `optb` text,
  `optc` text,
  `optd` text,
  `kunjaw` varchar(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bankso`
--

LOCK TABLES `bankso` WRITE;
/*!40000 ALTER TABLE `bankso` DISABLE KEYS */;
INSERT INTO `bankso` VALUES (1,7,1,'Who is Randi?','<img src=\'images/eng0701.png\'/>','He is an SMA studentâ€™s','He is a teacher ','He is a doctor','He is an SMP student','optd'),(2,7,2,'How many people are there in Mr .Rahmanâ€™s family?','<img src=\'images/eng0701.png\'/>','5','4','3','2','opta'),(3,7,3,'What is Randy`s mother?','<img src=\'images/eng0701.png\'/>','She is a programmer','She is a teacher','She is a student','She is a doctor','optb'),(4,7,4,'How many children does Mr. Rahman have?','<img src=\'images/eng0701.png\'/>','two','three','four','five','optb'),(5,7,5,'The main idea of the second paragraph is about','<img src=\'images/eng0701.png\'/>','The children in family','The jobs in family','The parents','The education','opta'),(6,7,6,'Rahmad : Wawan, this is Dharma. Dharma this is Wawan.<br/> Dharma  : How do you do, Wawan ?<br/>Wawan: ____',' ','How are you, Dharma?','How do you do, Dharma','What do you do, Dharma?','Nice to meet you, too','optb'),(7,7,7,'<p>Bejo : What is your ............ ?<br />Untung : My name is&nbsp;Sarmidi</p>',' ','Job','Address','Phone Number','Name','optd'),(8,7,8,'Michael : Where are you from?<br />Jordan : ____',' ','My name is Mike','I am from North Sulawesi','I am well, thanks a lot','I am from my grand mother','optb'),(9,7,9,'Michael : ___ ? <br />Jordan: I am from Chicago',' ','Where do you come from','Where are you','Where are you come from','Where are you from','optd'),(10,7,10,'How ____ players are in a basket ball team ?',' ','long','old','many','much','optc'),(11,6,1,'Ibu Kota Propinsi Nangro Aceh Darussalam',' ','Banda Aceh','Piddie','Sabang','Aceh Besar','opta'),(12,6,2,'Ibu Kota Propinsi Sumater Utara','<img src=\'images/ilustrasi2.jpg\'/>','Mandailing Natal','Medan','Asahan','Samosir','optb'),(13,6,3,'Danau di Sumatera Utara','<img src=\'images/toba-lake.png\'/>','Ranau','Asahan','Toba','Saguling','optc'),(14,6,4,'Ibu Kota Propinsi Sumatera Barat',' ','Limapuluh Kota','Padang Panjang','Palembang','Padang','optd'),(15,6,5,'Ibu Kota Propinsi Jambi',' ','Jambi','Muaro Jambi','Kerinci','Sorolangun','opta'),(16,6,6,'Ibu Kota Propinsi Bengkulu',' ','Lebong','Bengkulu','Rejang Lebong','Bengkalis','optb'),(17,6,7,'Ibu Kota Propinsi Sumatera Selatan',' ','Lubuk Linggau','Musirawas','Palembang','Prabumulih','optc'),(18,6,8,'Sungai di Propinsi Sumatera Selatan',' ','Mandailing','Mahakam','Musirawas','Musi','optd'),(19,6,9,'Ibu Kota Propinsi Riau',' ','Pekanbaru','Siak','Bengkalis','Dumai','opta'),(20,6,10,'Ibukota Provinsi Lampung',' ','Tanjung Pinang','Bandar Lampung','Kotabumi','Mesuji','optb'),(21,6,11,'Ibu Kota Propinsi Banteng',' ','Cengkareng','Merak','Tangerang','Serang','optc'),(22,6,12,'Ibu Kota Propinsi Jawa Barat',' ','Tasik Malaya','Bogor','Cirebon','Bandung','optd'),(23,6,13,'Ibu Kota Propinsi Jawa Tengah',' ','Semarang','Surakarta','Magelang','Purwokerto','opta'),(24,6,14,'Ibu Kota Propinsi DIY',' ','Sleman','Yogyakarta','Bantul','Kulon Progo','optb'),(25,6,15,'Ibu Kota Propinsi Jawa Timur',' ','Malang','Madiun','Surabaya','Gresik','optc'),(26,6,16,'Ibu Kota Propinsi Bali',' ','Buleleng','Ubud','Singaraja','Denpasar','optd'),(27,6,17,'Ibu Kota Propinsi NTB',' ','Mataram','Bima','Dompu','Sumbawa','opta'),(28,6,18,'Ibu Kota Propinsi NTT',' ','Manggarai','Kupang','Lembata','Alor','optb'),(29,6,19,'Ibu Kota Propinsi Kalimantan Barat',' ','Singkawang','Ketapang','Pontianak','Bengkayang','optc'),(30,6,20,'Ibu Kota Propinsi Kalimantan Selatan',' ','Kotabaru','Barito Kuala','Banjarbaru','Banjarmasin','optd'),(31,6,21,'Ibu Kota Propinsi Kalimantan Tengah',' ','Palangkaraya','Katingan','Seruyan','Murung Raya','opta'),(32,6,22,'Ibu Kota Propinsi Kalimantan Timur',' ','Kutai Timur','Samarinda','Bontang','Balikpapan','optb'),(33,6,23,'Ibukota Provinsi Sulawesi Selatan',' ','Enrekang','Jane Ponto','Makassar','Ujung Pandang','optc'),(34,6,24,'Nama Lain Kota Makassar',' ','Enrekang','Jane Ponto','Makassar','Ujung Pandang','optd'),(35,6,25,'Ibu Kota Propinsi Sulawesi Tengah',' ','Palu','Poso','Buol','Morowali','opta'),(36,6,26,'Ibu Kota Propinsi Sulawesi Tenggara',' ','Baubau','Kendari','Wakatobi','Kolaka','optb'),(37,6,27,'Ibu Kota Propinsi Sulawesi Utara',' ','Gorontalo','Minahasa','Manado','Sangihe','optc'),(38,6,28,'Ibu Kota Propinsi Gorontalo',' ','Boalemo','Bone Bolango','Puwuwato','Gorontalo','optd'),(39,6,29,'Ibu Kota Propinsi Maluku',' ','Ambon','Buru','Seram','Tual','opta'),(40,6,30,'Ibu Kota Propinsi Papua',' ','Jayawijaya','Jayapura','Biak Numfor','Yapen Waropen','optb'),(41,6,31,'Di Antara Pulau Kalimantan dan Sumatera Terdapat Selat ....',' ','Mahakam','Sunda','Karimata','Madura','optc'),(42,6,32,'Di Antara Pulau Jawa dan Madura Terdapat Selat ....',' ','Mahakam','Sunda','Karimata','Madura','optd'),(43,6,33,'Di Antara Pulau Jawa dan Bali Terdapat Selat ....',' ','Bali','Sunda','Karimata','Madura','opta'),(44,6,34,'Di Antara Pulau Jawa dan Sumatera Terdapat Selat ....',' ','Balikpapan','Sunda','Karimata','Madura','optb'),(45,6,35,'Fauna Identitas DKI Jakarta adalah',' ','Kijang','Burung Jalak','Elang Bondol','Elang Jawa','optc'),(46,6,36,'Berikut ini adalah fauna khas provinsi ....','<img src=\'https://www.binatangpeliharaan.org/wp-content/uploads/2016/02/suara-kepodang-emas.jpg\'/>','Jawa Tengah','DIY','Jawa Timur','Bali','opta'),(47,6,37,'Fauna Khas Daerah Jawa Barat Adalah',' ','<img src=\'http://www.gosumatra.com/wp-content/uploads/2013/05/Harimau-Sumatera-Panthera-Tigris-Sumatrae.jpg\'/>','<img src=\'http://www.naturephoto-cz.com/photos/maly/javan-leopard-42x_2212.jpg\'/>','<img src=\'http://cdn2.arkive.org/media/D7/D797681F-D80C-4AA2-9D70-457A862D8D57/Presentation.Large/Javan-leopard-black-morph.jpg\'/>','<img src=\'http://imgs.oomph.co.id/api_smart/data/shareit/Duh__Badak_Bercula_Satu_Hanya_Tinggal_57_Ekor-prev.jpg\'/>','optb'),(48,6,38,'Burung Berikut adalah satwa khas provinsi ...','<img src=\'http://2.bp.blogspot.com/-el9jo4u130M/VOnQ1FTkQeI/AAAAAAAAEZg/Jyi7-2U5zYQ/s1600/mp3-suara-burung-perkutut.jpg\'/>','Jawa Tengah','Bali','Daerah Istimewa Yogyakarta','Papua','optc'),(49,6,39,'Buah khas daerah Papua Berikut dan bisa jadi obat. Apa nama buah tersebut',' ','Buah lada hitam','Buah Lobak Merah','Buah Strawberry','Buah Merah','optd'),(50,6,40,'Pakaian adat berikut berasal dari pulau','<img src=\'http://budaya-indonesia.org/f/6074/roby08darisandi_sulbar.jpg\'/>','Sulawesi','Kalimantan','Maluku','Nusa Tenggara Barat','opta'),(55,10,1,'Pertanyaan No. 1','<img src=\'http://www.cadmiumpctech.co.uk/shop/image/cache/catalog/stickers/TuxBadge-600x600.jpg\'/>','Linux','Penguin','Kutub Utara','Suhu Dingin','opta'),(56,10,2,'Logo Debian',' ','<img src=\'https://news.opensuse.org/wp-content/uploads/2013/08/logo-evergreen1.png\'/>','<img src=\'http://iixmedia.com/blog/wp-content/uploads/2013/06/blankon.jpg.png\'/>','<img src=\'https://img.linuxadictos.com/wp-content/uploads/debian-logo1.jpg\'/>','<img src=\'http://cathbard.com/images/mint/mint-logo.svg\'/>','optc'),(57,10,3,'Mata Uang Jepang','<img src=\'\'/>','Dollar','Yen','Jika','Kalau','optb'),(58,10,4,'Pembuat Kernal Linux Pertama','<img src=\'http://10.21.122.93/nextcloud/index.php/apps/files_sharing/ajax/publicpreview.php?x=1366&y=303&a=true&file=alika.png&t=m3kHrjTdPv2ZArx&scalingup=0\'/>','Canonical','Ian Murdock','Richard Stalman','Linus Torvald','optd');
/*!40000 ALTER TABLE `bankso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `history`
--

DROP TABLE IF EXISTS `history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `history` (
  `logId` varchar(12) DEFAULT NULL,
  `psId` int(3) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `skore` int(3) DEFAULT NULL,
  UNIQUE KEY `logId` (`logId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `history`
--

LOCK TABLES `history` WRITE;
/*!40000 ALTER TABLE `history` DISABLE KEYS */;
INSERT INTO `history` VALUES ('tu004-001',7,'2017-03-20',26),('tu004-002',6,'2017-03-20',60),('tu001-001',7,'2017-03-20',18),('tu001-002',6,'2017-03-20',100),('tu002-001',10,'2017-04-16',12),('tu006-001',7,'2017-04-18',10),('tu002-002',7,'2017-04-19',22);
/*!40000 ALTER TABLE `history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mapel`
--

DROP TABLE IF EXISTS `mapel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mapel` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `namaMapel` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mapel`
--

LOCK TABLES `mapel` WRITE;
/*!40000 ALTER TABLE `mapel` DISABLE KEYS */;
INSERT INTO `mapel` VALUES (1,'Bahasa Indonesia'),(2,'Matematika'),(3,'Ilmu Pengetahuan Alam'),(4,'Ilmu Pengetahuan Sosial'),(5,'Basa Jawa'),(6,'TIK'),(7,'Al-Islam'),(8,'Baca Tulis Alquran'),(9,'Bahasa Inggris'),(10,'Pendidikan Kewarganegaraan'),(11,'Bahasa Arab'),(12,'Penjaskes'),(13,'Kertangkes');
/*!40000 ALTER TABLE `mapel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pakso`
--

DROP TABLE IF EXISTS `pakso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pakso` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `kmp` int(3) NOT NULL,
  `grade` int(2) DEFAULT '7',
  `nmp` int(2) NOT NULL,
  `kdg` varchar(20) NOT NULL,
  `startDay` date DEFAULT NULL,
  `startHour` time DEFAULT NULL,
  `endDay` date DEFAULT NULL,
  `endHour` time DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pakso`
--

LOCK TABLES `pakso` WRITE;
/*!40000 ALTER TABLE `pakso` DISABLE KEYS */;
INSERT INTO `pakso` VALUES (1,4,4,1,'G4001','2017-03-21','08:30:00','2017-03-21','09:00:00'),(2,1,5,1,'G5001','2017-03-21','08:00:00','2017-03-21','09:00:00'),(3,1,6,1,'G6001','2017-03-21','08:00:00','2017-03-21','09:00:00'),(4,2,6,1,'G6001','0000-00-00','00:00:00','0000-00-00','00:00:00'),(5,3,6,1,'G6001','0000-00-00','00:00:00','0000-00-00','00:00:00'),(6,4,6,1,'G6001','0000-00-00','00:00:00','0000-00-00','00:00:00'),(7,9,6,1,'G6001','0000-00-00','00:00:00','0000-00-00','00:00:00'),(8,3,6,2,'G6001','0000-00-00','00:00:00','0000-00-00','00:00:00'),(10,6,6,1,'G6002','2017-04-24','08:00:00','2017-04-29','14:00:00');
/*!40000 ALTER TABLE `pakso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` varchar(12) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `sex` enum('Laki-laki','Perempuan') DEFAULT 'Laki-laki',
  `position` enum('Siswa','Tutor','Admin') DEFAULT 'Siswa',
  `password` varchar(32) DEFAULT NULL,
  `grade` int(2) DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES ('2011-0001','Hanung Kamila Marva','Perempuan','Siswa','e174c5c4b2f00cd9cb8f76a87b41f6b2',6),('2011-0002','Anugrah Kamil Manaafi','Laki-laki','Siswa','115c1e83bfbe08372ab6b713fc482218',6),('G4001','Guru Kelas IVA','Perempuan','Tutor','00acf79f73b2cbb6ffc75a1c3bb733b9',4),('G5001','Guru Kelas VA','Laki-laki','Tutor','c8a9e42594637ba9d2ae23835f05d415',5),('G6001','Guru Kelas 6','Laki-laki','Tutor','e72ebb73eb0148cc6924b90ac542da3d',6),('G6002','Guru Kelas 6a','Perempuan','Tutor','9e8be8a7d36f3393308ee1561c3ed118',6),('tu001','User 001','Laki-laki','Siswa','aa13862c16500263fd7467534097f13a',6),('tu002','User 002','Perempuan','Siswa','e1c499828f05e14ef9c4b2ba52d8ba79',6),('tu003','User 003','Laki-laki','Siswa','403d5780a9348499a53c03e3b4cda0f7',6),('tu004','User 004','Perempuan','Siswa','d31add81b0bf87a617f9d4a113bca981',6),('tu005','User 005','Laki-laki','Siswa','120870076a2579f1e84e73e5b2eb1c60',6),('tu006','User 006','Perempuan','Siswa','0342279cd5f40568b348c2284c100752',6),('tu007','User 007','Laki-laki','Siswa','610d5a46964a7f9f30bd34d1e0ed3803',6),('tu008','User 008','Perempuan','Siswa','582ae62215724c695f1f979351f6fdb2',6),('tu009','User 009','Laki-laki','Siswa','14d20c9913d100779fbd760576c1830e',6),('tu010','User 010','Perempuan','Siswa','952ce7c7f20c6caeabe026887d850af5',6),('tu011','User 011','Laki-laki','Siswa','6c41376e364e6f7fb6536e2813e34be4',6),('tu012','User 012','Perempuan','Siswa','b273fe91bf619e5908f606f542700bda',6),('tu013','User 013','Laki-laki','Siswa','26a91196eec67b24d91f83b52bf9168c',6),('tu014','User 014','Perempuan','Siswa','dc19828c888544699c4b1001a02b0b38',6),('tu015','User 015','Laki-laki','Siswa','c97b8c89c7781c674651da7a52bbbcc1',6),('tu016','User 016','Perempuan','Siswa','a464538a12bdb5be8dc577132115077a',6),('tu017','User 017','Laki-laki','Siswa','402dc9c0fa52facae9e96d04bc7cd7e7',6),('tu018','User 018','Perempuan','Siswa','6dc95b410c0bd1269bbe7afac4b579f0',6),('tu019','User 019','Laki-laki','Siswa','d4e74b4cc8f183c6171d0d23614de277',6),('tu020','User 020','Perempuan','Siswa','f200eddcdc672e900a7f18124a4123b8',6),('tu021','User 021','Laki-laki','Siswa','5d06b2d929d2fa1aa88553ba46430ed0',6),('tu022','User 022','Perempuan','Siswa','cc13db1361aad20470c1478fa5a81787',6),('tu023','User 023','Laki-laki','Siswa','72d08de71600fe0d7937c27d9860f3f3',6),('tu024','User 024','Perempuan','Siswa','d8313a15f3d9358ea95f444498f48840',6),('tu025','User 025','Laki-laki','Siswa','f89c21aeefe0b001d3f29faa62480ac9',6),('tu026','User 026','Perempuan','Siswa','9b20082e7420db51653a23200e459fa6',6),('tu027','User 027','Laki-laki','Siswa','af59b261a2590fd1fe4fbf19d897e55f',6),('tu028','User 028','Perempuan','Siswa','c79fa01ee13be7e833215adc75f0ee6b',6),('tu029','User 029','Laki-laki','Siswa','aca65f1bc74dc4c5d586f90c4445c307',6),('tu030','User 030','Perempuan','Siswa','a235389b8ae19ff80aa4ea9f87e15bfc',6),('tu031','User 031','Laki-laki','Siswa','288dbc08ce3363a8a907489dcffc2ee4',6),('tu032','User 032','Perempuan','Siswa','b119162237bd7cb8a1fc6eb7afe2b5ed',6),('tu033','User 033','Laki-laki','Siswa','8669bc644fdf48085b40a5874cb8a8d9',6),('tu034','User 034','Perempuan','Siswa','280d1e30ceef28d88ede3f1774b4a2b5',6),('tu035','User 035','Laki-laki','Siswa','09c7a55108491786e1c4923c70b1cfaf',6),('tu036','User 036','Perempuan','Siswa','f3abfbd3378a8ad65128410a83404cc2',6),('tu037','User 037','Laki-laki','Siswa','7aae9dae90b516812b527c4336151254',6),('tu038','User 038','Perempuan','Siswa','8489d96b989d7ad79bf5fdcdac65ac3c',6),('tu039','User 039','Laki-laki','Siswa','62a25a3e5e302c5fd103ca795ad6e1da',6),('tu040','User 040','Perempuan','Siswa','906c05ab8d9a1457efb971cad0b4de3a',6);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `view_history`
--

DROP TABLE IF EXISTS `view_history`;
/*!50001 DROP VIEW IF EXISTS `view_history`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_history` (
  `logId` tinyint NOT NULL,
  `tanggal` tinyint NOT NULL,
  `namaMapel` tinyint NOT NULL,
  `nmp` tinyint NOT NULL,
  `skore` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_paketsoal`
--

DROP TABLE IF EXISTS `view_paketsoal`;
/*!50001 DROP VIEW IF EXISTS `view_paketsoal`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_paketsoal` (
  `id` tinyint NOT NULL,
  `mapel` tinyint NOT NULL,
  `grade` tinyint NOT NULL,
  `nmp` tinyint NOT NULL,
  `guru` tinyint NOT NULL,
  `startDay` tinyint NOT NULL,
  `startHour` tinyint NOT NULL,
  `endDay` tinyint NOT NULL,
  `endHour` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `view_history`
--

/*!50001 DROP TABLE IF EXISTS `view_history`*/;
/*!50001 DROP VIEW IF EXISTS `view_history`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`banksoal`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_history` AS select `history`.`logId` AS `logId`,`history`.`tanggal` AS `tanggal`,`mapel`.`namaMapel` AS `namaMapel`,`pakso`.`nmp` AS `nmp`,`history`.`skore` AS `skore` from ((`history` join `mapel`) join `pakso`) where ((`pakso`.`id` = `history`.`psId`) and (`mapel`.`id` = `pakso`.`kmp`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_paketsoal`
--

/*!50001 DROP TABLE IF EXISTS `view_paketsoal`*/;
/*!50001 DROP VIEW IF EXISTS `view_paketsoal`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`banksoal`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_paketsoal` AS select `pakso`.`id` AS `id`,`mapel`.`namaMapel` AS `mapel`,`pakso`.`grade` AS `grade`,`pakso`.`nmp` AS `nmp`,`user`.`nama` AS `guru`,`pakso`.`startDay` AS `startDay`,`pakso`.`startHour` AS `startHour`,`pakso`.`endDay` AS `endDay`,`pakso`.`endHour` AS `endHour` from ((`pakso` join `mapel`) join `user`) where ((`mapel`.`id` = `pakso`.`kmp`) and (`user`.`id` = `pakso`.`kdg`) and (`user`.`position` = 'Tutor')) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-19 19:48:34
