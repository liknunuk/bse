CREATE OR REPLACE VIEW view_paketsoal AS
SELECT pakso.id id, mapel.namaMapel mapel, pakso.grade, nmp, user.nama guru, startDay,startHour,endDay,endHour 
FROM pakso,mapel,user 
WHERE mapel.id = pakso.kmp && user.id=pakso.kdg && user.position='Tutor';
