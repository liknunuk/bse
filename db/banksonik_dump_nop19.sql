-- MySQL dump 10.17  Distrib 10.3.17-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: banksonik
-- ------------------------------------------------------
-- Server version	10.3.17-MariaDB-0+deb10u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bankso`
--

DROP TABLE IF EXISTS `bankso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bankso` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `psId` int(4) NOT NULL,
  `nso` int(2) NOT NULL,
  `pertanyaan` text DEFAULT NULL,
  `ilustrasi` text DEFAULT NULL,
  `opta` text DEFAULT NULL,
  `optb` text DEFAULT NULL,
  `optc` text DEFAULT NULL,
  `optd` text DEFAULT NULL,
  `opte` text DEFAULT NULL,
  `kunjaw` varchar(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bankso`
--

LOCK TABLES `bankso` WRITE;
/*!40000 ALTER TABLE `bankso` DISABLE KEYS */;
INSERT INTO `bankso` VALUES (1,1,1,'Pertanyaan No. 1','<img src=\'http://www.cadmiumpctech.co.uk/shop/image/cache/catalog/stickers/TuxBadge-600x600.jpg\'/>','Jawaban A','Jawaban B','Jawaban C','Jawaban D','Jawaban E','opta'),(2,1,2,'Logo Debian',' ','<img src=\'https://news.opensuse.org/wp-content/uploads/2013/08/logo-evergreen1.png\'/>','<img src=\'http://iixmedia.com/blog/wp-content/uploads/2013/06/blankon.jpg.png\'/>','<img src=\'https://img.linuxadictos.com/wp-content/uploads/debian-logo1.jpg\'/>','<img src=\'http://cathbard.com/images/mint/mint-logo.svg\'/>','Tidak Tahu','optc'),(3,1,3,'Mata Uang Jepang','<img src=\'\'/>','Jawaban A','Jawaban B','Jawaban C','Jawaban D','Jawaban E','optb'),(4,1,4,'Mata Uang Jepang','<img src=\'\'/>','Jawaban A','Jawaban B','Jawaban C','Jawaban D','Jawaban E','optb'),(5,1,5,'Mata Uang Jepang','<img src=\'\'/>','Jawaban A','Jawaban B','Jawaban C','Jawaban D','Jawaban E','optb'),(6,1,6,'Mata Uang Jepang','<img src=\'\'/>','Jawaban A','Jawaban B','Jawaban C','Jawaban D','Jawaban E','optb'),(7,1,7,'Mata Uang Jepang','<img src=\'\'/>','Jawaban A','Jawaban B','Jawaban C','Jawaban D','Jawaban E','optb'),(8,1,8,'Mata Uang Jepang','<img src=\'\'/>','Jawaban A','Jawaban B','Jawaban C','Jawaban D','Jawaban E','optb'),(9,1,9,'Mata Uang Jepang','<img src=\'\'/>','Jawaban A','Jawaban B','Jawaban C','Jawaban D','Jawaban E','optb'),(10,1,10,'Mata Uang Jepang','<img src=\'\'/>','Jawaban A','Jawaban B','Jawaban C','Jawaban D','Jawaban E','optb'),(11,1,11,'Mata Uang Jepang','<img src=\'\'/>','Jawaban A','Jawaban B','Jawaban C','Jawaban D','Jawaban E','optb'),(12,1,12,'Mata Uang Jepang','<img src=\'\'/>','Jawaban A','Jawaban B','Jawaban C','Jawaban D','Jawaban E','optb'),(13,1,13,'Mata Uang Jepang','<img src=\'\'/>','Jawaban A','Jawaban B','Jawaban C','Jawaban D','Jawaban E','optb'),(14,1,14,'Mata Uang Jepang','<img src=\'\'/>','Jawaban A','Jawaban B','Jawaban C','Jawaban D','Jawaban E','optb'),(15,1,15,'Mata Uang Jepang','<img src=\'\'/>','Jawaban A','Jawaban B','Jawaban C','Jawaban D','Jawaban E','optb'),(16,1,16,'Mata Uang Jepang','<img src=\'\'/>','Jawaban A','Jawaban B','Jawaban C','Jawaban D','Jawaban E','optb'),(17,1,17,'Mata Uang Jepang','<img src=\'\'/>','Jawaban A','Jawaban B','Jawaban C','Jawaban D','Jawaban E','optb'),(18,1,18,'Mata Uang Jepang','<img src=\'\'/>','Jawaban A','Jawaban B','Jawaban C','Jawaban D','Jawaban E','optb'),(19,1,19,'Mata Uang Jepang','<img src=\'\'/>','Jawaban A','Jawaban B','Jawaban C','Jawaban D','Jawaban E','optb'),(20,1,20,'Mata Uang Jepang','<img src=\'\'/>','Jawaban A','Jawaban B','Jawaban C','Jawaban D','Jawaban E','optb');
/*!40000 ALTER TABLE `bankso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `history`
--

DROP TABLE IF EXISTS `history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `history` (
  `logId` varchar(12) DEFAULT NULL,
  `psId` int(3) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `skore` int(3) DEFAULT NULL,
  UNIQUE KEY `logId` (`logId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `history`
--

LOCK TABLES `history` WRITE;
/*!40000 ALTER TABLE `history` DISABLE KEYS */;
/*!40000 ALTER TABLE `history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mapel`
--

DROP TABLE IF EXISTS `mapel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mapel` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `namaMapel` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mapel`
--

LOCK TABLES `mapel` WRITE;
/*!40000 ALTER TABLE `mapel` DISABLE KEYS */;
INSERT INTO `mapel` VALUES (1,'Uji Coba - Kelas X'),(2,'Uji Coba Kelas XI'),(3,'Uji Coba Kelas XII');
/*!40000 ALTER TABLE `mapel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pakso`
--

DROP TABLE IF EXISTS `pakso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pakso` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `kmp` int(3) NOT NULL,
  `grade` int(2) DEFAULT 7,
  `nmp` int(2) NOT NULL,
  `kdg` varchar(20) NOT NULL,
  `startDay` date DEFAULT NULL,
  `startHour` time DEFAULT NULL,
  `endDay` date DEFAULT NULL,
  `endHour` time DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pakso`
--

LOCK TABLES `pakso` WRITE;
/*!40000 ALTER TABLE `pakso` DISABLE KEYS */;
INSERT INTO `pakso` VALUES (1,1,10,1,'G003','2019-09-16','07:00:00','2019-09-16','12:00:00');
/*!40000 ALTER TABLE `pakso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` varchar(12) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `sex` enum('Laki-laki','Perempuan') DEFAULT 'Laki-laki',
  `position` enum('Siswa','Tutor','Admin') DEFAULT 'Siswa',
  `password` varchar(32) DEFAULT NULL,
  `grade` int(2) DEFAULT 0,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES ('G001','Guru Contoh 01','Laki-laki','Tutor','3cf57d28e7ad5cc86c89f7a417d1a4f1',12),('G002','Guru Contoh 02','Perempuan','Tutor','027dc622cc6009e7cc494f33db39eaf5',11),('G003','Guru Contoh 03','Laki-laki','Tutor','929e328a475163006f3893ba52116e01',10),('S1001','Siswa Kelas 10 Absen 1','Perempuan','Siswa','d7561ba7169856cef6e492bfc6020d10',10),('S1101','Siswa Kelas 11 Absen 01','Laki-laki','Siswa','090251dcd85e9c095e0b289ad320cb21',11),('S1201','Siswa Kelas 12 Absen 01','Laki-laki','Siswa','5602c0e56a04458799b2212f19059d95',12);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `view_history`
--

DROP TABLE IF EXISTS `view_history`;
/*!50001 DROP VIEW IF EXISTS `view_history`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_history` (
  `logId` tinyint NOT NULL,
  `tanggal` tinyint NOT NULL,
  `namaMapel` tinyint NOT NULL,
  `nmp` tinyint NOT NULL,
  `skore` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_paketsoal`
--

DROP TABLE IF EXISTS `view_paketsoal`;
/*!50001 DROP VIEW IF EXISTS `view_paketsoal`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_paketsoal` (
  `id` tinyint NOT NULL,
  `mapel` tinyint NOT NULL,
  `grade` tinyint NOT NULL,
  `nmp` tinyint NOT NULL,
  `guru` tinyint NOT NULL,
  `startDay` tinyint NOT NULL,
  `startHour` tinyint NOT NULL,
  `endDay` tinyint NOT NULL,
  `endHour` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `view_history`
--

/*!50001 DROP TABLE IF EXISTS `view_history`*/;
/*!50001 DROP VIEW IF EXISTS `view_history`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`banksoal`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_history` AS select `history`.`logId` AS `logId`,`history`.`tanggal` AS `tanggal`,`mapel`.`namaMapel` AS `namaMapel`,`pakso`.`nmp` AS `nmp`,`history`.`skore` AS `skore` from ((`history` join `mapel`) join `pakso`) where `pakso`.`id` = `history`.`psId` and `mapel`.`id` = `pakso`.`kmp` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_paketsoal`
--

/*!50001 DROP TABLE IF EXISTS `view_paketsoal`*/;
/*!50001 DROP VIEW IF EXISTS `view_paketsoal`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`banksoal`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_paketsoal` AS select `pakso`.`id` AS `id`,`mapel`.`namaMapel` AS `mapel`,`pakso`.`grade` AS `grade`,`pakso`.`nmp` AS `nmp`,`user`.`nama` AS `guru`,`pakso`.`startDay` AS `startDay`,`pakso`.`startHour` AS `startHour`,`pakso`.`endDay` AS `endDay`,`pakso`.`endHour` AS `endHour` from ((`pakso` join `mapel`) join `user`) where `mapel`.`id` = `pakso`.`kmp` and `user`.`id` = `pakso`.`kdg` and `user`.`position` = 'Tutor' */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-20  8:57:58
