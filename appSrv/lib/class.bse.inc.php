<?php
define('server','http://likgopar.mugeno.org/ngh/banksonik/server/images');
class banksonic{
	function transact($sql){
		include('koneksi.inc.php');
		$qry = $conn->prepare($sql);
		return $qry;
	}
	
	function pilihMapel(){
		$sql = "SELECT * FROM mapel ORDER BY namaMapel";
		$qry = $this->transact($sql);
		$qry->execute();
		$mapel = array();
		while($rs = $qry->fetch())
		{
			$pel=array('id'=>$rs['id'],'nama'=>$rs['namaMapel']);
			array_push($mapel,$pel);
		}
		return $mapel;
	}
	
	function pilihPaket($mp,$kl=6){
		$sql = "SELECT 	pakso.id id, nmp, count(psId) soal, startDay,
						startHour,endDay,endHour 
				FROM pakso, bankso  
				WHERE	kmp = ? && pakso.grade=? && 
						bankso.psId=pakso.id";
		$qry = $this->transact($sql);
		$qry->execute(array($mp,$kl));
		$pakso = array();
		while($rs = $qry->fetch())
		{
			$start  = $rs['startDay'].' '.$rs['startHour'];
			$finish = $rs['endDay'].' '.$rs['endHour'];
			$paket=array('id'=>$rs['id'],'np'=>$rs['nmp'],'soal'=>$rs['soal'],
						 'start'=>$start,'finish'=>$finish);
			array_push($pakso,$paket);
		}
		return $pakso;
	}
	
	function gelarSoal($id,$u){
		//Check Valid Period
		$olih = $this->validityPeriod($id);
		if($olih == '0'){
			$soal = array();
			$quiz=array('qs'=>'Paket soal tidak bisa dikerjakan saat ini',
						'il'=>'','a'=>'','b'=>'','c'=>'','d'=>'','k'=>'');
			array_push($soal,$quiz);
			return($soal);
			exit();
		}
		
		$sql = "SELECT pertanyaan,ilustrasi,opta,optb,optc,optd,opte,kunjaw
			FROM bankso WHERE psId= ? && nso = ?";
		$qry = $this->transact($sql);
		$qry->execute(array($id,$u));
		$soal = array();
		while($rs = $qry->fetch())
		{
			if($rs['ilustrasi']==''){
				$il=$rs['ilustrasi'];
			}else{
				$il=$this->isLocal($rs['ilustrasi']);
				//echo $il;
			}
			$quiz=array('qs'=>$rs['pertanyaan'],'il'=>$il,'a'=>$rs['opta'],
				    'b'=>$rs['optb'],'c'=>$rs['optc'],'d'=>$rs['optd'],'e'=>$rs['opte'],
				    'k'=>$rs['kunjaw']);
			array_push($soal,$quiz);
		}
		return $soal;
	}
	
	function validityPeriod($psid){
		$sql ="SELECT startDay sd, startHour sh, endDay ed, endHour eh
			   FROM pakso WHERE id = ? LIMIT 1";
		$qry = $this->transact($sql);
		$qry->execute(array($psid));
		$rs = $qry->fetch();
		$start =$rs['sd'].' '.$rs['sh'];
		$finsh =$rs['ed'].' '.$rs['eh'];
		$now = date('Y-m-d H:i:s');
		if($start == '0000-00-00 00:00:00'){
			return('1');
		}else{
			if($now <= $start  || $now >=$finsh ){
				return('0');
			}else{
				return('1');
			}
		}
		
		
	}
		
	function isLocal($il){
		//<img src='images
		if(substr($il,10,6)=='images'){
			$nil=substr_replace($il,server,10,6);
			return $nil;
		}else{
			return $il;
		}
	}
	
	function siswaLogin($uid,$pwd){
		$sql = "SELECT id,nama,grade
				FROM user WHERE password = md5('".$uid."><".$pwd."')
				LIMIT 1";
		//echo $sql;
		$qry = $this->transact($sql);
		$qry->execute(array());
		$r = $qry->fetch();
		if($r == NULL){
			return "user tidak ditemukan";
		}else{
			return($r['id'].'-'.$r['nama'].'-'.$r['grade']);
		}
	}
	
	function trialHistory($pkt,$uid,$scr){
		$now = date('Y-m-d');
		$log = $this->trialLogId($uid);
		$sql = "INSERT INTO history
				SET logId = ?, psId = ? , tanggal = ? , skore = ?";
		$qry = $this->transact($sql);
		$qry->execute(array($log,$pkt,$now,$scr));
		return("Nomor Log Latihan Soal: ".$log);
	}
	
	function trialLogId($uid)
	{
		$sql = "SELECT COUNT(logId) log 
				FROM history
				WHERE logId LIKE ? ";
		$usr = $uid."-%";
		$qry = $this->transact($sql);
		$qry->execute(array($usr));
		$r = $qry->fetch();
		$seq = $r['log']+1;
		$seq = sprintf("%03d",$seq);
		return($uid.'-'.$seq);
	}
	
	function historyList($uid){
		
		$sql = "SELECT history.*,view_paketsoal.mapel 
				FROM history,view_paketsoal
				WHERE	view_paketsoal.id=history.psId &&
						history.logId LIKE ?
				ORDER BY history.logId DESC LIMIT 20";
		$log = $uid."-%";
		$qry = $this->transact($sql);
		$qry->execute(array($log));
		$logData = array();
		while($r = $qry->fetch()){
			$tanggal = $this->balikTanggal($r['tanggal']);
			$data = array('logId'=>$r['logId'],
						  'mapel'=>$r['mapel'],
						  'tanggal'=>$tanggal,
						  'score'=>$r['skore']);
			array_push($logData,$data);
		}
		return($logData);
	}
	
	function balikTanggal($tgl){
		list($y,$m,$d)=explode("-",$tgl);
		return(	sprintf("%02d",$d).'/'.
				sprintf("%02d",$m).'/'.
				sprintf("%04d",$y));
	}
}
?>
